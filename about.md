---
layout: page
title: Acerca de este beta
permalink: /about/
---

La mesa está puesta, la felicidad está pesada. Welcome to the last estrato de lo underground. Acompañanos a [@TanelVizcaya](https://twitter.com/TanelVizcaya), [@sebsmartinez08](https://twitter.com/sebsmartinez08) y [@razon_voz](https://twitter.com/razon_voz) en este viaje onírico de autodescubrimiento y drugs.  


Tambien nos puedes encontrar por estos medios y no olvides seguir el feed:  

+ [Feed Podcast](https://sinpresiones.gitlab.io/feed)  
+ [Twitter](https://twitter.com/SinPressure)  
+ [Correo](nohaypresion@gmail.com)  
+ [Web](https://sinpresiones.gitlab.io/)  
+ [Telegram](https://t.me/)
+ [Facebook](https://www.facebook.com/SinPressure/)